package exceptionTask;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.*;

public class Main {
    private static void myClosable() throws Exception {
        try (Closable myClosable = new Closable()) {
            myClosable.works();
        }
    }

    public static void main(String[] args) {
        boolean a;
        try (Scanner scanner = new Scanner(System.in)) {
            a = scanner.hasNext();
        } catch (InputMismatchException e) {
            throw new WrongValue();
        }
        if (a != true || a != false) {
            throw new NotBooleanValue();
        }
    }
        Main.myClosable();

}
