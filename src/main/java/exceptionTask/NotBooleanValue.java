package exceptionTask;


public class NotBooleanValue extends Exception {
    public NotBooleanValue() {
    super("Your keyboard sends bad letters which not equal to boolean values((");
    }

    public NotBooleanValue(String message) {
        super(message);
    }

    public NotBooleanValue(String message, Throwable cause) {
        super(message, cause);
    }

    public NotBooleanValue(Throwable cause) {
        super(cause);
    }
}
