package book;

public enum Menu {
    By_year(1, "Enter 1, if you want to find books after this year"),
    By_author(2, "Enter 2, if you want to find books by author"),
    By_publisher(3, "Enter 3, if you want to find books by publisher"),
    EXIT(4, "Enter 4 to exit");
    private final int index;
    private final String msg;

    Menu(int index, String msg) {
        this.index = index;
        this.msg = msg;
    }

    public int getIndex() {
        return index;
    }

    public String getMsg() {
        return msg;
    }
    public static void showMenu(){
        System.out.println("Do your choise");
        for(Menu menu : Menu.values()
        ){
            System.out.println(menu.getIndex()+ ":"+ menu.getMsg());
        }
    }
    public static Menu getIndexOfChoice(int index){
        for (Menu menu : Menu.values()
        ){
          if (menu.getIndex()==index){
              return menu;
          }
        }return null;
    }

}


