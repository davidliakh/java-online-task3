package book;

public class Book {
    private int id;
    private String name;
    private String author;
    private String publisher;
    private int year;
    private int lists;
    private double price;
    private String type;

    public Book(int id, String name, String author, String publisher, int year, int lists, double price, String type) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
        this.lists = lists;
        this.price = price;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getLists() {
        return lists;
    }

    public void setLists(int lists) {
        this.lists = lists;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return "ID: " + id + "\n" + "Name: " + name + "\n" + "Author: " + author + "\n" + "Publiher: " + publisher + "\n" + "Year: " + year + "\n" + "Number of lists: " + lists + "\n" + "Price: " + price + "\n" + "Type of binding: " + type;
    }
}
