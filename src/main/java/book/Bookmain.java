package book;

public class Bookmain {
   static Bookutils utils = new Bookutils();

    public static void main(String[] args) {
        Book book1 = new Book(1, "Math", "Ivan", "Ukraine", 2013, 313, 149.9, "Normal");
        Book book2 = new Book(2, "Chemistry", "Kuchma", "Ukraine", 2010, 259, 155.5, "Normal");
        Book book3 = new Book(3, "Literature", "Yanukovich", "Ukraine", 2015, 200, 212.0, "Normal");
        Book book4 = new Book(4, "Music", "Yakubovih", "Ukraine", 2016, 300, 167.8, "Normal");
        Book book5 = new Book(5, "Law", "Sokyrka", "Ukraine", 2014, 356, 122.5, "Normal");
        Book book6 = new Book(6, "Physics", "Ivanka", "Ukraine", 2006, 316, 200.0, "Normal");
        Book book7 = new Book(7, "Drawing", "Sarakhman", "Ukraine", 2019, 300, 137.6, "Normal");

        utils.getBooks().add(book1);
        utils.getBooks().add(book2);
        utils.getBooks().add(book3);
        utils.getBooks().add(book4);
        utils.getBooks().add(book5);
        utils.getBooks().add(book6);
        utils.getBooks().add(book7);

        utils.executeStatement();
    }
}