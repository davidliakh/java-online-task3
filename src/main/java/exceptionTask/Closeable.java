package exceptionTask;

public class Closeable implements AutoCloseable {
    @Override
    public void close() throws MyException{
        throw new MyException();
    }
    public void works() {
        System.out.println("MyClosable worked!");
    }
}
