package book;

import java.util.*;

public class Bookutils {


    Scanner sc = new Scanner(System.in);
    private final List<Book> books = new ArrayList<Book>();


    public List<Book> getBooks() {
        return books;
    }


    private List<Book> findByYear() {
        System.out.println("Enter year:");
        double minLength = Double.valueOf(sc.nextInt());

        List<Book> booksbyyear = new ArrayList<Book>();
        for (Book book : books) {
            if (book.getYear() >= minLength) {
                booksbyyear.add(book);
            } else {
                System.out.println("There is no books in this year");
            }
        }

        return booksbyyear;
    }


    public String defineBypublisher() {
        String author=sc.nextLine();
        for (Book book : books){
            if (author.equals(book.getPublisher())){
                 author = author;
            }
        }return author;

    }

    public void executeStatement() {
        while (true) {
            Menu.showMenu();
            Integer index = Integer.valueOf(sc.nextInt());
            Menu menu = Menu.getIndexOfChoice(index);
            switch (menu) {
                case By_year:
                    findByYear();
                    break;
                case By_author:
                    break;
                case By_publisher:
                    defineBypublisher();
                    break;
                case EXIT:
                    System.out.println("Bye!");
                    return;
                default:
                    System.out.println("There is no such option!");
                    break;
            }
        }
    }
}