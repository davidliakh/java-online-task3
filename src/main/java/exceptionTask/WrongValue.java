package exceptionTask;

public class WrongValue extends Exception {
    public WrongValue() {
        super("You entered wrong value, please be sure about your input");
    }

    public WrongValue(String message) {
        super(message);
    }

    public WrongValue(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongValue(Throwable cause) {
        super(cause);
    }
}
